--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion:  29/11/2018
--@Descripcion: Proyecto Final. Creacion de tablas extenas para realizar
-- consultas posteriores

prompt ============ Creacion de tablas externas ============

prompt Conectando como usuario SYS
connect sys as sysdba

whenever sqlerror exit rollback

prompt Creando directorio temporal
create or replace directory sucursales_dir as '/proyecto_final/empresa';

grant read, write on directory sucursales_dir to o_proy_admin;

prompt Conectando como administrador
connect o_proy_admin

whenever sqlerror exit rollback

prompt Creando tabla externa
create table sucursales_ext(
  sucursal_id number(4,0),
  estado varchar2(40),
  municipio varchar(40),
  fecha_apertura date
)
organization external (
  type oracle_loader
  default directory sucursales_dir
  access parameters (
    records delimited by newline
  badfile sucursales_dir:'sucursales_ext_bad.log'
  logfile sucursales_dir:'sucursales_ext.log'
  fields terminated by ';'
  lrtrim
  missing field values are null
    (
    sucursal_id,
    estado,
    municipio,
    fecha_apertura date mask "dd/mm/yyyy"
    )
  )
  location ('sucursales.csv')
  )
reject limit unlimited;

prompt Listo!

disconnect
