--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creación: 4/12/2018
--Descripción: Proyecto Final. Trigger que cambia el tipo de pago
-- cuando se eliminan datos de la tarjeta de un cliente

prompt ============ Trigger cambios en forma de pago ============


prompt Conectando como administrado
connect o_proy_admin

create or replace trigger trg_tarjeta_id
  after update of forma_pago on servicio
  for each row
  declare
    v_clienteID number;
  begin
    v_clienteID := :old.cliente_id;
    if :old.forma_pago='T' then
      delete from tarjeta where cliente_id=v_clienteID;
    end if;
  end;
/
show errors

disconnect
