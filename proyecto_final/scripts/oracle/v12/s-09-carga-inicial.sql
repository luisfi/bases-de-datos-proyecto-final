--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion:  29/11/2018
--@Descripcion: Proyecto Final. Carga inical de datos para comprobar el
-- correcto funcionamiento de la base

prompt ============ Cargando datos ============

prompt Conectando como administrador
connect o_proy_admin

whenever sqlerror exit rollback
set serveroutput on


prompt Creando funcion random para enteros
create or replace function random_integer(lower number, upper number)
  return number is
begin
  return round(dbms_random.value(lower, upper));
end;
/
show errors

prompt Creando funcion para generar fechas aleatorias
create or replace function random_date(lower number, upper number)
  return date is
  v_dia number;
  v_mes number;
  v_anio number;
begin
  v_dia := random_integer(1, 27);
  v_mes := random_integer(1, 11);
  v_anio := random_integer(lower, upper);
  return to_date(v_dia||'/'||v_mes||'/'||v_anio, 'dd/mm/yyyy');
end;
/
show errors

prompt ==> Borrando datos de tablas
declare
  v_nombreTabla user_tables.table_name%type;
  v_numTablas number;
  cursor cur_tabla is
    select * from user_tables;
begin
  select count(*)
    into v_numTablas
    from user_tables;

  if v_numTablas > 0 then
    for reg in cur_tabla loop
      if reg.table_name = 'SUCURSALES_EXT' then
        continue;
      end if;
      execute immediate 'delete from ' || reg.table_name;
    end loop;
  end if;
end;
/
show errors
commit;

prompt ==> Insertando datos de los estados
insert into estado (estado_id, clave, nombre)
values(1, 'MEX', 'ESTADO DE MEXICO');
insert into estado (estado_id, clave, nombre)
values(2, 'DF', 'CDMX');
insert into estado (estado_id, clave, nombre)
values(3, 'CHS', 'CHIAPAS');
insert into estado (estado_id, clave, nombre)
values(4, 'JAL', 'JALISCO');
insert into estado (estado_id, clave, nombre)
values(5, 'PUE', 'PUEBLA');
commit;

prompt ==> Insertando datos de municipios
insert into municipio (municipio_id, clave, nombre, estado_id)
values (1, 'ECA', 'ECATEPEC DE MORELOS', 1);
insert into municipio (municipio_id, clave, nombre, estado_id)
values (2, 'ZMP', 'ZUMPANGO', 1);
insert into municipio (municipio_id, clave, nombre, estado_id)
values (3, 'GAM', 'GUSTAVO A. MADERO', 2);
insert into municipio (municipio_id, clave, nombre, estado_id)
values (4, 'ZPN', 'ZAPOPAN', 4);
commit;

prompt ==> Insertando Tipos de Canales
insert into tipo_canal (tipo_canal_id, clave, descripcion)
values (1, 'A', 'PARA TODO EL PUBLICO');
insert into tipo_canal (tipo_canal_id, clave, descripcion)
values (2, 'B', 'PARA ADOLECENTES Y ADULTOS');
insert into tipo_canal (tipo_canal_id, clave, descripcion)
values (3, 'C', 'ADULTOS');
commit;

prompt ==> Creando los estatus en los que se puede encontrar el servicio
insert into estatus_servicio (estatus_servicio_id, nombre, descripcion)
values (1, 'CREADO', 'CONTRATADO POR PRIMERA VEZ');
insert into estatus_servicio (estatus_servicio_id, nombre, descripcion)
values (2, 'VIGENTE', 'PRIMER PAGO REALIZADO');
insert into estatus_servicio (estatus_servicio_id, nombre, descripcion)
values (3, 'CON ADEUDO', 'PAGO ATRASADO');
insert into estatus_servicio (estatus_servicio_id, nombre, descripcion)
values (4, 'SUSPENDIDO', 'MAS DE 15 DIAS DE ATRASO');
insert into estatus_servicio (estatus_servicio_id, nombre, descripcion)
values (5, 'CONCLUIDO CON ADEUDO', 'ATRASO DE MAS DE UN ANIO');
insert into estatus_servicio (estatus_servicio_id, nombre, descripcion)
values(6, 'CONCLUIDO', 'CONTRATO FINALIZADO');
commit;

prompt ==> Insertando Canales
declare
  v_numeroCanal canal.canal_id%type;
  v_nombreCanal canal.nombre%type;
  v_descripcionCanal canal.descripcion%type;
  v_tipoCanalID tipo_canal.tipo_canal_id%type; -- [1,3] rango tipo_canal_id
begin
  v_numeroCanal  := canal_seq.nextval;
  v_nombreCanal := 'CANAL_';
  v_descripcionCanal := 'DESCRIPCION DE CANAL_';
  v_tipoCanalID := random_integer(1,3);

  for i in 1..200 loop
    -- dbms_output.put_line('Creando canal ' || v_nombreCanal || v_numeroCanal);
    insert into canal (canal_id,
                       numero,
                       nombre,
                       descripcion,
                       tipo_canal_id)
    values (v_numeroCanal,
            v_numeroCanal,
            v_nombreCanal||v_numeroCanal,
            v_descripcionCanal||v_numeroCanal,
            v_tipoCanalID );
    v_numeroCanal := canal_seq.nextval;
    v_tipoCanalID := random_integer(1,3);
  end loop;
end;
/
show errors
commit;

prompt ==> Creando Paquetes de TV
declare
    v_inicio number;
    v_limite number;
begin
  select canal_id
    into v_inicio
    from canal
    where numero=(select min(numero)
                    from canal);
  for i in 1..3 loop
    if i = 1 then
      dbms_output.put_line('Creando paquete basico');
      v_limite := v_inicio + 50;
      insert into paquete_tv(paquete_tv_id, clave, descripcion, costo)
      values (i, 'BA', 'BASICO', 489);
    elsif i = 2 then
      dbms_output.put_line('Creando paquete intermedio');
      v_limite := v_inicio + 100;
      insert into paquete_tv(paquete_tv_id, clave, descripcion, costo)
      values (i, 'IN', 'INTERMEDIO', 665);
    elsif i = 3 then
      dbms_output.put_line('Creando paquete completo');
      v_limite := v_inicio + 150;
      insert into paquete_tv(paquete_tv_id, clave, descripcion, costo)
      values (i, 'CO', 'COMPLETO', 999);
    end if;
    for canal in v_inicio..v_limite loop
      insert into paquete_canal_tv (paquete_canal_tv_id, canal_id, paquete_tv_id)
      values (paquete_canal_tv_seq.nextval, canal, i);
    end loop;
  end loop;
end;
/
show errors

prompt ==> Registrando clientes
declare
  v_clienteID number;
  v_servicioID number;
  v_numDireccion number;
  v_tipoTarjeta tarjeta.tipo%type;
  v_tipoPago servicio.forma_pago%type;
  v_fechaRegistro date;
  v_estatusServicioId estatus_servicio.estatus_servicio_id%type;
begin

  for c_id in 1..20 loop
    -- dbms_output.put_line('Cliente ' || c_id);
    -- dbms_output.put_line('--> Registrando datos');
    v_tipoPago := 'E';
    -- Registrando datos de cliente
    v_clienteID := cliente_seq.nextval;
    insert into cliente (cliente_id,
                         nombre,
                         apellido_paterno,
                         apellido_materno,
                         rfc,
                         curp)
    values (v_clienteID,
            'CLIENTE_'||v_clienteID,
            'AP_PAT_CLI_'||v_clienteID,
            'AP_MAT_CLI_'||v_clienteID,
            'RFC_CLI_'||v_clienteID,
            'CURP_CLI_'||v_clienteID );

    -- Registrando direcciones
    -- dbms_output.put_line('--> Registrando direccion');
    insert into direccion (numero,
                           cliente_id,
                           calle,
                           colonia,
                           numero_exterior,
                           codigo_postal,
                           municipio_id)
    values (1,
            v_clienteID,
            'CALLE_CLI_'||v_clienteID,
            'COL_CLI_'||v_clienteID,
            random_integer(1, 1000),
            random_integer(10000, 100000),
            random_integer(1, 4) );

    -- Se deja a la suerte si se registran 2 direcciones
    if random_integer(0,1) = 1 then
      --ToDo: Cambiar a codigo postal
      -- dbms_output.put_line('--> Registrando segunda direccion');
      insert into direccion (numero,
                             cliente_id,
                             calle,
                             colonia,
                             numero_exterior,
                             codigo_postal,
                             municipio_id)
      values (2,
              v_clienteID,
              'CALLE2_CLI_'||v_clienteID,
              'COL2_CLI_'||v_clienteID,
              random_integer(1, 1000),
              random_integer(10000, 100000),
              random_integer(1, 4) );
    end if;

    -- Registrando tarjeta (suerte)
    if random_integer(0,1) = 1 then
      -- dbms_output.put_line('--> Registrando tarjeta');
      v_tipoPago := 'T';
      case random_integer(1, 3)
        when 1 then
          v_tipoTarjeta := 'VISA';
        when 2 then
          v_tipoTarjeta := 'MASTER CARD';
        when 3 then
          v_tipoTarjeta := 'AMERICAN EXPRESS';
      end case;
      insert into tarjeta (tarjeta_id,
                           numero,
                           tipo,
                           numero_seguridad,
                           cliente_id)
      values (tarjeta_seq.nextval,
              to_char(random_integer(1000000000000000, 9999999999999999)),
              v_tipoTarjeta,
              random_integer(100, 999),
              v_clienteID );
    end if;

    -- dbms_output.put_line('--> Registrando servicios');
    select estatus_servicio_id
      into v_estatusServicioId
      from estatus_servicio where nombre='CREADO';

    v_servicioID := servicio_seq.nextval;
    v_fechaRegistro := sysdate;

    -- Algunos usuarios se crean con deuda para ejercicio de procedimientos
    if random_integer(0,1) = 1 then
      v_fechaRegistro := random_date(2000, 2005);
      select estatus_servicio_id
        into v_estatusServicioId from estatus_servicio
        where nombre='SUSPENDIDO';
    end if;

    --Registrando servicios
    -- ToDo: Trigger verficar que al menos se registra un servicio
    -- ToDo: Trigger que obtiene la tarifa mensual
    -- ToDo: Duda fecha corte
    insert into servicio (servicio_id,
                          numero_cuenta,
                          tarifa_mensual,
                          forma_pago,
                          fecha_corte,
                          fecha_instalacion,
                          es_tv, es_telefono, es_internet,
                          fecha_estatus,
                          estatus_servicio_id,
                          cliente_id,
                          anios_contrato)
    values (v_servicioID,
            servicio_numero_cuenta_seq.nextval,
            random_integer(489, 1500),
            v_tipoPago,
            v_fechaRegistro,
            v_fechaRegistro + 8,
            0, 0, 0,
            v_fechaRegistro, --ToDo: Duda como se registran los catalogos
            v_estatusServicioId,
            v_clienteID,
            random_integer(1,5));

    insert into estatus_servicio_historial (estatus_servicio_historial_id,
                                            fecha_estatus,
                                            servicio_id,
                                            estatus_servicio_id)
    values (estatus_servicio_historial_seq.nextval,
            v_fechaRegistro,
            v_servicioID,
            v_estatusServicioId );

    -- dbms_output.put_line('---- Registrando servicio de telefono');
    insert into servicio_telefono(servicio_id,
                                  numero_llamadas,
                                  numero_asignado)
    values (v_servicioID,
            random_integer(100, 500),
            random_integer(1000000000, 5599999999) );

    update servicio set es_telefono=1
      where servicio_id = v_servicioID;

    if random_integer(0,1) = 1 then
      -- dbms_output.put_line('---- Registrando servicio de television');
      insert into servicio_tv (servicio_id,
                               numero_decodificadores,
                               paquete_tv_id)
      values (v_servicioID,
              random_integer(1, 5),
              random_integer(1, 3) );
      update servicio set es_tv=1
        where servicio_id = v_servicioID;
    end if;

    if random_integer(0,1) = 1 then
      -- dbms_output.put_line('---- Registrando servicio de internet');
      insert into servicio_internet(servicio_id,
                                    velocidad_bajada,
                                    nombre_correo,
                                    password_correo)
      values(v_servicioID,
             random_integer(10, 100),
             dbms_random.string('X', random_integer(5, 20))||'@inet.com',
             dbms_random.string('U', 30) );
      update servicio set es_internet=1
        where servicio_id = v_servicioID;
    end if;
  end loop;
end;
/
show errors

prompt ==> Registrando pagos a los clientes

declare
  v_fechaRegistro date;
  cursor cur_servicio is
    select servicio_id,
           fecha_instalacion,
           tarifa_mensual
      from servicio;
begin
  for s in cur_servicio loop
    v_fechaRegistro := s.fecha_instalacion;
    for i in 1..random_integer(10,20) loop
      insert into pago (folio,
                        servicio_id,
                        fecha,
                        importe)
      values (pago_folio_seq.nextval,
              s.servicio_id,
              v_fechaRegistro + 30,
              s.tarifa_mensual );
    end loop;
  end loop;
end;
/
show errors
prompt Listo!

disconnect
