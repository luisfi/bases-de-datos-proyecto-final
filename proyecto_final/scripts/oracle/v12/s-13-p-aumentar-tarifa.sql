--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion: 4/12/2018
--@Descripción: Proyecto Final. Procedimiento almacenado para aumentar x
-- porcentaje al pago de todos los usuarios

prompt ============ Procedimiento almacenado para cambiar tarifa ============

prompt Conectando como administrador
connect o_proy_admin

create or replace procedure aumentarTarifaMensual(porcentaje number) is
  cursor cur_servicio is
  select * from servicio;
begin
  for s in cur_servicio loop
    update servicio set tarifa_mensual=(s.tarifa_mensual + s.tarifa_mensual*porcentaje)
      where servicio_id=s.servicio_id;
  end loop;
end;
/
show errors
disconnect
