--@Fecha creacion: 4/12/2018
--@Descripción: Proyecto Final. Prueba de la funcion que calcula
-- ingresos promedio

@@s-15-fx-ingresos-promedio.sql

prompt ============ Prueba Funcion  ============

prompt Conectando como administrador
connect o_proy_admin

set serveroutput on

declare
  v_promedio number;
begin
  v_promedio := calcularIngresoPromedio(2000);
  dbms_output.put_line('Ingresos promedio del año 2000: '||v_promedio);
  v_promedio := calcularIngresoPromedio(2001);
  dbms_output.put_line('Ingresos promedio del año 2001: '||v_promedio);
  v_promedio := calcularIngresoPromedio(2002);
  dbms_output.put_line('Ingresos promedio del año 2002: '||v_promedio);
  v_promedio := calcularIngresoPromedio(2003);
  dbms_output.put_line('Ingresos promedio del año 2003: '||v_promedio);
  v_promedio := calcularIngresoPromedio(2004);
  dbms_output.put_line('Ingresos promedio del año 2004: '||v_promedio);
  v_promedio := calcularIngresoPromedio(2019);
  dbms_output.put_line('Ingresos promedio del año 2019: '||v_promedio);
end;
/
show errors
