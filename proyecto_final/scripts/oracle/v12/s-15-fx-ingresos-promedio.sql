--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion: 4/12/2018
--@Descripción: Proyecto Final. Funcion que calcula el promedio de
-- ingresos en un año, recibe el año como parametro

prompt ============ Funcion que carga calcula promedio de ingresos ============

prompt Conectando como administrador
connect o_proy_admin

set serveroutput on

create or replace function calcularIngresoPromedio(anio in number)
  return number is
  cursor cur_pago is
    select importe
    from pago
    where extract(year from fecha)=anio;
  v_promedio number;
  v_contador number;
begin
  v_promedio := 0;
  v_contador := 0;
  for p in cur_pago loop
    v_promedio := v_promedio + p.importe;
    v_contador := v_contador + 1;
  end loop;
  if v_contador = 0 then
    return 0;
  else
    return v_promedio/v_contador;
  end if;
end;
/
show errors

disconnect
