--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creación: 4/12/2018
--Descripción: Proyecto Final. Prueba para mostrar la actualizacion del
-- estatus del servicio en su historial

@s-11-tr-historial.sql

prompt ============ Prueba trigger ============


prompt Conectando como administrador
connect o_proy_admin

whenever sqlerror exit rollback
set serveroutput on

declare
  v_clienteID number;
  v_nombreEstatus estatus_servicio.nombre%type;
  v_nombreCliente cliente.nombre%type;
  v_servicioID number;
  cursor cur_historialEstatus is
  select h.servicio_id,
         es.nombre,
         h.fecha_estatus
    from estatus_servicio es,
         estatus_servicio_historial h
    where es.estatus_servicio_id=h.estatus_servicio_id and
          h.servicio_id=(select min(servicio_id) from servicio);
begin
  select min(servicio_id) into v_servicioID from servicio;
  select cliente_id, nombre
    into v_clienteID, v_nombreCliente
    from cliente
    where cliente_id=(select cliente_id
                        from servicio
                        where servicio_id=v_servicioID);
  select nombre
    into v_nombreEstatus
    from estatus_servicio
    where estatus_servicio_id=(select estatus_servicio_id from servicio where servicio_id=v_servicioID);

  dbms_output.put_line('Servicio ID: ' || v_servicioID);
  dbms_output.put_line('Cliente ID: ' || v_servicioID || ' ' || v_nombreCliente);
  dbms_output.put_line('Estatus actual: ' || v_nombreEstatus);
  dbms_output.put_line('Historial estatus: ');
  for estatus in cur_historialEstatus loop
      dbms_output.put_line('Fecha estatus: ' || estatus.fecha_estatus || ' ' || estatus.nombre);
  end loop;
  dbms_output.put_line('Actualizando su estado a Vigente');
  update servicio set estatus_servicio_id = (select estatus_servicio_id from estatus_servicio where nombre='VIGENTE'),
                      fecha_estatus=sysdate
    where servicio_id=v_servicioID;

  dbms_output.put_line('Nuevo historial estatus: ');
  for estatus in cur_historialEstatus loop
    dbms_output.put_line('Fecha estatus: ' || estatus.fecha_estatus || ' ' || estatus.nombre);
  end loop;
end;
/
show errors

disconnect
