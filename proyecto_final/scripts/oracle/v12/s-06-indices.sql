--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion:  29/11/2018
--@Descripcion: Proyecto Final. Creacion de indices para mejorar las
-- busquedas


prompt ============ Creacion de indices ============

prompt Conectando como administrador
connect o_proy_admin

whenever sqlerror exit rollback

create unique index tarjeta_numero_uk on tarjeta(numero);

create index servicio_cliente_ix on servicio(cliente_id);

create index cliente_datos_uk on cliente(rfc, nombre, apellido_paterno);

create unique index cliente_nombre_uk on cliente(upper(nombre));

prompt Listo!

disconnect
