--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion: 4/12/2018
--@Descripción: Proyecto Final. Procedimiento almacenado que actualiza el
-- estatus de todos aquellos servicios que tienen un año o mas de atraso

prompt ============ Procedimiento almacenado paramarcar adeudos ============

prompt Conectando como administrador
connect o_proy_admin

create or replace procedure concluirConAdeudo is
  cursor cur_servicio is
    select *
    from servicio s
    join estatus_servicio es
    on s.estatus_servicio_id=es.estatus_servicio_id
    where es.nombre='SUSPENDIDO';
  v_estatusConAdeudo number;
begin
  select estatus_servicio_id
    into v_estatusConAdeudo
    from estatus_servicio
    where nombre='CONCLUIDO CON ADEUDO';
  for s in cur_servicio loop
    if sysdate - s.fecha_estatus>= 365 then
      dbms_output.put_line('--> El servicio ' || s.servicio_Id);
      dbms_output.put_line('Cliente ' || s.cliente_id);
      dbms_output.put_line('Tiene ' || to_char(sysdate - s.fecha_estatus) || ' dias de atraso');
      dbms_output.put_line('Se procedera legalmente.');
      update servicio set estatus_servicio_id=v_estatusConAdeudo where servicio_id=s.servicio_id;
    end if;
  end loop;
end;
  /
show errors

disconnect;

select * from estatus_servicio_historial;;
