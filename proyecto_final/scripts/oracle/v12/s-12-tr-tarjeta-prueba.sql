--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creación: 4/12/2018
--Descripción: Proyecto Final. Prueba para mostrar la actualizacion
-- de la tarjeta al cambiar la forma de pago

@@s-11-tr-tarjeta.sql

prompt ============ Prueba trigger ============


prompt Conectando como administrador o_proy_admin
connect o_proy_admin

whenever sqlerror exit rollback
set serveroutput on

declare
  v_servicioID number;
  v_clienteNombre cliente.nombre%type;
  v_clienteID number;
  v_numeroTarjetas number;
begin
  select min(servicio_id)
    into v_servicioID
    from (select servicio_id from servicio where forma_pago='T');
  select cliente_id
    into v_clienteID
    from servicio
    where servicio_id=v_servicioID;
  select nombre
    into v_clienteNombre
    from cliente
    where cliente_id=v_clienteID;
  select count(*)
    into v_numeroTarjetas
    from tarjeta
    where cliente_id=v_clienteID;
  dbms_output.put_line('Cliente ' || v_clienteID || ' ' || v_clienteNombre);
  dbms_output.put_line('Tarjetas: ' || v_numeroTarjetas);
  dbms_output.put_line('Modificando forma de pago a E');
  update servicio set forma_pago='E' where servicio_id=v_servicioID;
  commit;
  dbms_output.put_line('Consultando nuevamente:');
  select count(*)
    into v_numeroTarjetas
    from tarjeta
    where cliente_id=v_clienteID;
  dbms_output.put_line('Tarjetas: ' || v_numeroTarjetas);
end;
/
disconnect
