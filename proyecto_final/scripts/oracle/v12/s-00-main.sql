--@Autor:   Luis Alberto Oropeza Vilchis
--@Fecha creacion:  29/11/2018
--@Descripcion: Script principal donde se realiza la ejecucion de los demas
--  scripts.

prompt Conectando como usuario SYS
connect sys as sysdba

set serveroutput on

prompt ============ Eliminando usuarios en caso de existir ============
declare
  v_count number(1,0);
begin
  -- Eliminando usuario o_proy_admin
  select count(*)
  into v_count
  from dba_users
  where username = 'O_PROY_ADMIN';

  if v_count > 0 then
    execute immediate 'drop user O_PROY_ADMIN cascade';
    dbms_output.put_line('Usuario o_proy_admin eliminado');
  end if;

  select count(*)
  into v_count
  from dba_users
  where username = 'O_PROY_INVITADO';

  if v_count > 0 then
    execute immediate 'drop user O_PROY_INVITADO cascade';
    dbms_output.put_line('Usuario o_proy_invitado eliminado');
  end if;

  select count(*)
  into v_count
  from dba_roles
  where role = 'ROL_ADMIN';

  if v_count > 0 then
    execute immediate 'drop role ROL_ADMIN';
    dbms_output.put_line('Rol de administrador eliminador');
  end if;

  select count(*)
  into v_count
  from dba_roles
  where role = 'ROL_INVITADO';

  if v_count > 0 then
    execute immediate 'drop role ROL_INVITADO';
    dbms_output.put_line('Rol de invitado eliminado');
  end if;
end;
/

@@s-01-usuarios.sql
@@s-02-entidades.sql
@@s-03-tablas-temporales.sql
@@s-04-tablas-externas.sql
@@s-05-secuencias.sql
@@s-06-indices.sql
@@s-07-sinonimos.sql
@@s-08-vistas.sql
@@s-09-carga-inicial.sql
@@s-10-consultas.sql
--@@s-11-tr-historial.sql
--@@s-11-tr-tarjeta.sql
--@@s-11-tr-cambio-paquete.sql
@@s-12-tr-historial-prueba.sql
@@s-12-tr-tarjeta-prueba.sql
@@s-12-tr-cambio-paquete-prueba.sql
-- @@s-13-p-adeudo.sql
-- @@s-13-p-agregar-tarjeta.sql
-- @@s-13-p-aumentar-tarifa.sql
@@s-14-p-adeudo-prueba.sql
@@s-14-p-agregar-tarjeta-prueba.sql
@@s-14-p-aumentar-tarifa-prueba.sql
-- @@s-15-fx-carga-blob.sql
-- @@s-15-ingresos-promedio.sql
@@s-16-fx-carga-blob-prueba.sql
@@s-16-ingresos-promedio-prueba.sql
@@s-resultados-proyecto-final.sql

prompt Ejecucion de main finalizada!

disconnect
