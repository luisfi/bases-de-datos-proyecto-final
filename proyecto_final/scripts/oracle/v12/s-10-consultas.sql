--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion:  4/12/18
--@Descripcion: Proyecto Final. Se realizan consultas sobre los datos creados
-- utilizando joins, funciones de agregacion, subconsultas, etc.

prompt ============ Consultas ============

prompt Conectando como administrador
connect o_proy_admin


promp Consultado catalogos (all)
select * from estado;

select * from municipio;

select * from tipo_canal;

select * from canal;

select * from paquete_tv;

select * from estatus_servicio;

prompt consultando datos de usuario (right y left join)
select c.nombre,
       c.apellido_paterno,
       c.apellido_materno,
       c.rfc,
       c.curp,
       t.numero,
       t.tipo,
       d.numero,
       d.calle,
       d.colonia,
       d.numero_exterior,
       d.codigo_postal,
       m.nombre,
       e.nombre
from estado e
join municipio m using(estado_id)
join direccion d using (municipio_id)
right join cliente c
on d.cliente_id=c.cliente_id
left join tarjeta t
on c.cliente_id=t.cliente_id;


prompt Consulta de servicios por usuario (left join)
select c.nombre,
       c.rfc,
       s.numero_cuenta,
       s.tarifa_mensual,
       stv.numero_decodificadores,
       stel.numero_llamadas,
       stel.numero_asignado as num_telefono,
       sint.velocidad_bajada,
       sint.nombre_correo
from cliente c
join servicio s
on c.cliente_id=s.cliente_id
left join servicio_tv stv
on s.servicio_id=stv.servicio_id
left join servicio_telefono stel
on s.servicio_id=stel.servicio_id
left join servicio_internet sint
on s.servicio_id=sint.servicio_id;

prompt Consulta de canales con su tipo (natural join)
select c.numero,
       c.nombre,
       c.descripcion,
       tc.descripcion as tipo_canal
from canal c
join tipo_canal tc using(tipo_canal_id);


prompt Mostrando numero de clientes por estado (funcion de agregacion count)
select e.estado_id,
       e.nombre,
       count(*)
from estado e
join municipio m
on e.estado_id=m.estado_id
join direccion d
on m.municipio_id=d.municipio_id
join cliente c
on d.cliente_id=c.cliente_id
group by e.estado_id,
         e.nombre;


prompt Mostrando numero de canales por tipo
select tc.tipo_canal_id as ID,
       tc.clave,
       tc.descripcion,
       count(*) as numero_canales
from tipo_canal tc
join canal c
on tc.tipo_canal_id=c.tipo_canal_id
group by tc.tipo_canal_id,
         tc.clave,
         tc.descripcion;


prompt Seleccionando a todos los clientes con correo excluyendo a los de la CDMX
select sint.nombre_correo
from cliente c,
     servicio s,
     servicio_internet sint
where c.cliente_id=s.cliente_id and
      s.servicio_id=sint.servicio_id
minus
select sint.nombre_correo
from cliente c,
     servicio s,
     servicio_internet sint,
     direccion d,
     municipio m,
     estado e
where sint.servicio_id=s.servicio_id and
      s.cliente_id=c.cliente_id and
      c.cliente_id=d.cliente_id and
      d.municipio_id=m.municipio_id and
      m.estado_id=e.estado_id and
      e.estado_id= (select estado_id from estado where clave='DF');

prompt Listo!

disconnect
