--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion: 4/12/2018
--@Descripción: Proyecto Final. Procedimiento almacenado para agregar el
-- servicio de internet a un cliente

prompt ============ Procedimiento almacenado para agregar tarjeta ============


prompt Conectando como administrador
connect o_proy_admin

create or replace procedure agregarTarjetaCliente(clienteID in number,
                                                    numTarjeta in varchar2,
                                                    tipoTarjeta in varchar2,
                                                    numSeguridad in number) is
cursor cur_servicio is
  select * from servicio
    where cliente_id=clienteID;
begin
  insert into tarjeta (tarjeta_id, numero, tipo, numero_seguridad, cliente_id)
    values (tarjeta_seq.nextval, numTarjeta, tipoTarjeta, numSeguridad, clienteID);
  for s in cur_servicio loop
    update servicio set forma_pago='T' where servicio_id=s.servicio_id;
  end loop;
end;
/
show errors
disconnect
