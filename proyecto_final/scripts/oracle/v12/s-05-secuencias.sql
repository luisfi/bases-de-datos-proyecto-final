--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creación: 30/10/2018
--Descripción: Proyecto Final. Creación de las secuencias para insertar datos

prompt ============ Creación de secuencias ============

prompt Conectando como administrador
connect o_proy_admin

whenever sqlerror exit rollback

create sequence servicio_seq
start with 1
increment by 1
nocycle
cache 1000
order;

create sequence pago_folio_seq
start with 1001 -- ToDo: Checar conflicto con minval
increment by 1
minvalue 1000
maxvalue 1e10
cycle;

create sequence cliente_seq
start with 100
increment by 1
nocycle
cache 100
order;

create sequence tarjeta_seq
start with 20
increment by 2
nocycle;

create sequence estatus_servicio_seq
start with 1
increment by 1
nocycle
noorder;

create sequence estatus_servicio_historial_seq
start with 1
increment by 1
cache 10;

create sequence canal_seq
start with 1
maxvalue 10000
increment by 1
nocycle;

create sequence tipo_canal_seq
start with 1
increment by 1
nocycle;

create sequence paquete_canal_tv_seq
start with 1
increment by 1
nocycle;

create sequence paquete_tv_seq
start with 10
maxvalue 100
increment by 1
nocycle;

create sequence servicio_tv_canal_seq
start with 10
increment by 2
nocycle;

create sequence servicio_numero_cuenta_seq
start with 1
increment by 1
nocycle;

prompt Listo!

disconnect
