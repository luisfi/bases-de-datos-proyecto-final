--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creación: 4/12/2018
--Descripción: Proyecto Final. Pruebas para el trigger de cambios en costo

@s-11-tr-cambio-paquete.sql

prompt ============ Prueba trigger ============

prompt Conectando como administrador
connect o_proy_admin

whenever sqlerror exit rollback
set serveroutput on

declare
  cursor cur_paquete_tv is
  select * from paquete_tv;

  cursor cur_cambio is
  select * from paquete_tv_cambio;
begin
  dbms_output.put_line('--> Cambios en paquetes:');
  for c in cur_cambio loop
    dbms_output.put_line(c.paquete_tv_id || ' ' || c.costo_anterior || ' ---> ' || c.costo_nuevo);
  end loop;
  dbms_output.put_line('--> Paquetes:');
  for p in cur_paquete_tv loop
    dbms_output.put_line('Paquete ' || p.paquete_tv_id || ' ' || p.clave || ' ' || p.descripcion);
    dbms_output.put_line('Aumentando 10% de costo');
    update paquete_tv set costo = p.costo*1.1 where paquete_tv_id=p.paquete_tv_id;
  end loop;
  dbms_output.put_line('--> Despues de hacer update: ');
  for c in cur_cambio loop
    dbms_output.put_line(c.paquete_tv_id || ' ' || c.costo_anterior || ' ---> ' || c.costo_nuevo);
  end loop;
end;
/
show errors

disconnect
