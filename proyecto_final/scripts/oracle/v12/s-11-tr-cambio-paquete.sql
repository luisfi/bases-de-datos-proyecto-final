--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creación: 4/12/2018
--Descripción: Proyecto Final. Trigger que almacena los cambios en los
-- paquetes de television

prompt ============ Trigger cambios en paquete TV ============

whenever sqlerror exit rollback
set serveroutput on

prompt Conectando como administrador
connect o_proy_admin

prompt Creando tabla para poder utilizar el trigger
declare
  v_contador number;
begin
  select count(*) into v_contador from user_tables where table_name='PAQUETE_TV_CAMBIO';
  if v_contador = 1 then
    dbms_output.put_line('Tabla existente. Borrando...');
    execute immediate 'drop table paquete_tv_cambio';
  end if;
end;
/
show errors

create table paquete_tv_cambio (
  paquete_tv_cambio_id number(10,0),
  paquete_tv_id number(10,0) not null,
  costo_anterior number(7,2) not null,
  costo_nuevo number(7,2) not null,
  fecha_actualizacion date not null,
  usuario_actualizacion varchar(40) not null,
  constraint paquete_tv_cambio_pk primary key(paquete_tv_cambio_id)
);

prompt Creando secuencia para los cambios
declare
  v_contador number;
begin
  select count(*) into v_contador from user_sequences where sequence_name='PAQUETE_TV_CAMBIO_SEQ';
  if v_contador = 1 then
    dbms_output.put_line('Secuencia existente. Borrando...');
    execute immediate 'drop sequence paquete_tv_cambio_seq';
  end if;
end;
/
show errors

create sequence paquete_tv_cambio_seq
start with 1
increment by 1
nocycle;

prompt Creando trigger
create or replace trigger trg_paquete_tv
  for update of costo on paquete_tv
  compound trigger

  type paquete_actualizado_tipo is record (
    paquete_tv_cambio_id paquete_tv_cambio.paquete_tv_cambio_id%type,
    paquete_tv_id paquete_tv_cambio.paquete_tv_id%type,
    costo_anterior paquete_tv_cambio.costo_anterior%type,
    costo_nuevo paquete_tv_cambio.costo_nuevo%type,
    fecha_actualizacion paquete_tv_cambio.fecha_actualizacion%type,
    usuario_actualizacion paquete_tv_cambio.usuario_actualizacion%type
  );

  type paquete_lista_tipo is table of paquete_actualizado_tipo;

  paquete_lista paquete_lista_tipo := paquete_lista_tipo();

  before each row  is
    v_usuario varchar2(30) := sys_context('USERENV', 'SESSION_USER');
    v_fecha date := sysdate;
    v_indice number;
  begin
    paquete_lista.extend;
    v_indice := paquete_lista.last;
    paquete_lista(v_indice).paquete_tv_cambio_id := paquete_tv_cambio_seq.nextval;
    paquete_lista(v_indice).paquete_tv_id := :new.paquete_tv_id;
    paquete_lista(v_indice).costo_anterior := :old.costo;
    paquete_lista(v_indice).costo_nuevo := :new.costo;
    paquete_lista(v_indice).fecha_actualizacion := v_fecha;
    paquete_lista(v_indice).usuario_actualizacion := v_usuario;
  end before each row;

  after statement is
  begin
    forall i in paquete_lista.first .. paquete_lista.last
      insert into paquete_tv_cambio (paquete_tv_cambio_id, paquete_tv_id,
                                      costo_anterior, costo_nuevo, fecha_actualizacion,
                                      usuario_actualizacion)
      values (paquete_lista(i).paquete_tv_cambio_id, paquete_lista(i).paquete_tv_id,
              paquete_lista(i).costo_anterior, paquete_lista(i).costo_nuevo,
              paquete_lista(i).fecha_actualizacion, paquete_lista(i).usuario_actualizacion);
  end after statement;
end;
/
show errors

disconnect
