--@Autor:  Luis Alberto Oropeza Vilchis
--@Fecha creacion:  29/11/2018
--@Descripcion: Proyecto Final. Creacion de tablas temporales

prompt ============ Creacion de tablas temporales ============

prompt Conectando como administrador
connect o_proy_admin

whenever sqlerror exit rollback

create global temporary table reporte_servicio (
  servicio_id number(10, 0),
  tarifa_mensual number(5,6),
  forma_pago varchar2(1),
  numero_decodificadores number(2,0),
  numero_llamadas number(5,0),
  velocidad_bajada number(4,0),
  velocidad_subida number(4, 0)
) on commit delete rows;


create global temporary table cliente_preferente (
    cliente_id number(10,0),
    nombre varchar2(40),
    apellido_paterno varchar2(40),
    apellido_materno varchar2(40),
    numero_tarjeta varchar2(16),
    tipo_tarjeta varchar2(1)
) on commit preserve rows;

prompt Listo!

disconnect
