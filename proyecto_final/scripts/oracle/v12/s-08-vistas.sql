--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion:  29/11/2018
--@Descripcion: Proyecto Final. Creacion de vistas para poder ocultar
-- informacion o complejidad de consultas


prompt ============ Creando vistas ============

prompt Conectando como administrador
connect o_proy_admin

whenever sqlerror exit rollback

create or replace view v_cliente (
  cliente_id,
  nombre,
  apellido_paterno,
  numero_cuenta,
  forma_pago,
  clave_paquete_tv,
  numero_decodificadores,
  numero_llamadas,
  numero_asignado,
  velocidad_bajada,
  velocidad_subida,
  correo)
  as select c.cliente_id,
            c.nombre,
            c.apellido_paterno,
            s.numero_cuenta,
            s.forma_pago,
            ptv.clave,
            stv.numero_decodificadores,
            stel.numero_llamadas,
            stel.numero_asignado,
            sint.velocidad_bajada,
            sint.velocidad_subida,
            sint.nombre_correo
  from cliente c
  join servicio s
  on c.cliente_id=s.cliente_id
  left join servicio_internet sint
  on s.servicio_id=sint.servicio_id
  left join servicio_telefono stel
  on s.servicio_id=stel.servicio_id
  left join servicio_tv stv
  on s.servicio_id=stel.servicio_id
  left join paquete_tv ptv
  on stv.paquete_tv_id=ptv.paquete_tv_id;


create or replace view v_tarjeta (
  cliente_id,
  nombre_cliente,
  apellido_paterno,
  tipo_tarjeta
) as select c.cliente_id,
            c.nombre,
            c.apellido_paterno,
            t.tipo
from cliente c,
     tarjeta t
where c.cliente_id=t.cliente_id;

create or replace view v_paquete_tv (
  paquete_tv_id,
  clave,
  costo,
  numero_canales
  ) as select ptv.paquete_tv_id,
              ptv.clave,
              ptv.costo,
              count(*)
from paquete_tv ptv,
     paquete_canal_tv pctv
where ptv.paquete_tv_id=pctv.paquete_tv_id
group by ptv.paquete_tv_id,
              ptv.clave,
              ptv.costo;

prompt Listo!

disconnect
