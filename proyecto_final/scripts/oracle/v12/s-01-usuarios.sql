--@Autor:   Luis Alberto Oropeza Vilchis
--@Fecha creacion:      30/10/2018
--@Descripcion:   Creacion de los usuarios en la base de datos

prompt ============ Creacion de usuarios ============
prompt Conectando como usuario SYS
connect sys as sysdba

prompt Creando usuario o_proy_admin
create user o_proy_admin
identified by luis quota unlimited
on users password expire;

prompt Creando usuario o_proy_invitado
create user o_proy_invitado identified by luis;

prompt Creando roles de administrador e invitado
create role rol_admin;
grant create table,
      create view,
      create synonym,
      create public synonym,
      create sequence,
      create trigger,
      create procedure,
      create session
to rol_admin;

create role rol_invitado;
grant create session
to rol_invitado;

prompt Asignando roles a los usuarios
grant rol_admin to o_proy_admin;
grant rol_invitado to o_proy_invitado;

prompt Listo!

disconnect
