--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion: 4/12/2018
--@Descripción: Proyecto Final. Prueba al procedimiento almacenado

@@s-13-p-aumentar-tarifa.sql

prompt ============ Prueba procedimiento almacenado  ============

prompt Conectando como administrador
connect o_proy_admin

set serveroutput on

declare
  cursor cur_servicio is
    select * from servicio
    where forma_pago='T';
begin
  dbms_output.put_line('--> Antes del aumento: ');
  for s in cur_servicio loop
    dbms_output.put_line(s.servicio_id || ' --> ' || s.tarifa_mensual);
  end loop;
  aumentarTarifaMensual(0.10);
  dbms_output.put_line('--> Despues del aumento: ');
  for s in cur_servicio loop
    dbms_output.put_line(s.servicio_id || ' --> ' || s.tarifa_mensual);
  end loop;
end;
/
show errors

disconnect
