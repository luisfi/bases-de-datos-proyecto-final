--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creación:  30/10/2018
--Descripción:  Creación de las tablas para el caso de estudio  iNet

/*
ToDos:
  * Check numero direccion --
  * Check tipo canal --
  * Check tipo tarjeta --
  * check tipo pago --
  * check tipo paquete_tv --
  * Unique correo electronico --
  * Unique numero telefono --
  * Default tipo de pago --
  * Default fecha corte --
  * Default fecha estatus --
  * Velocidad de subida virtual --
  * Se agrega años de contrato, para generar fecha fin como virtual --
  * Secuencia para el folio --
  * Funcion para checar numero de telefono --
  * Funcion para revisar correo --
 */

prompt ============ Creando tablas ============
prompt Conectando como administrador
connect o_proy_admin

create table estatus_servicio (
  estatus_servicio_id number(10,0),
  nombre varchar2(40) not null,
  descripcion varchar2(400) not null,
  constraint estatus_servicio_pk primary key(estatus_servicio_id)
);

create table cliente (
  cliente_id number(10,0),
  nombre varchar2(40) not null,
  apellido_paterno varchar2(40) not null,
  apellido_materno varchar2(40),
  rfc varchar2(13) not null,
  curp varchar2(18),
  constraint cliente_pk primary key(cliente_id)
);

create table tarjeta (
  tarjeta_id number(10,0),
  numero varchar2(16) not null,
  tipo varchar2(25) not null,
  numero_seguridad number(3,0) not null,
  cliente_id number(10,0) not null,
  constraint tarjeta_pk primary key(tarjeta_id),
  constraint tarjeta_cliente_id_fk foreign key(cliente_id)
    references cliente(cliente_id) on delete cascade,
  constraint tarjeta_tipo_chk check ( tipo in('VISA', 'MASTER CARD', 'AMERICAN EXPRESS') )
);

create table estado (
  estado_id number(3,0),
  clave varchar2(3) not null,
  nombre varchar2(40) not null,
  constraint estado_pk primary key(estado_id)
);

create table municipio (
  municipio_id number(5,0),
  nombre varchar2(40),
  clave varchar2(40) not null,
  constraint municipio_pk primary key(municipio_id),
  estado_id not null constraint municipio_estado_id_fk
    references estado(estado_id) on delete cascade
);

-- Tener cuidado al definir constraints a veces buscas por palabra eg foreign key references
create table direccion (
  numero number(1,0),
  cliente_id number(10,0),
  calle varchar2(40) not null,
  colonia varchar2(40) not null,
  numero_interior number(4,0),
  numero_exterior number(4,0)not null,
  codigo_postal number(5,0) not null,
  municipio_id number(5,0) not null,
  constraint direccion_municipio_id_fk foreign key(municipio_id)
    references municipio(municipio_id) on delete cascade,
  constraint direccion_cliente_id_fk foreign key(cliente_id)
    references cliente(cliente_id) on delete cascade,
  constraint direccion_pk primary key(cliente_id, numero),
  constraint direccion_numero_chk check ( numero in (1, 2))
);

create table tipo_canal (
  tipo_canal_id number(3,0),
  clave varchar2(5) not null,
  descripcion varchar2(400) not null,
  constraint tipo_canal_pk primary key(tipo_canal_id),
  constraint tipo_canal_clave_chk check ( clave in ('A', 'B', 'C') )
);

create table canal (
  canal_id number(10,0),
  numero number(6,0) not null,
  nombre varchar2(40) not null,
  descripcion varchar2(400) not null,
  tipo_canal_id not null,
  constraint canal_pk primary key(canal_id),
  constraint canal_tipo_canal_id_fk foreign key(tipo_canal_id)
    references tipo_canal(tipo_canal_id) on delete cascade
);


create table paquete_tv (
  paquete_tv_id number(3,0), --ToDo: cambiar tipo en diagrama
  clave varchar2(2) not null, --ToDo: cambiar en diagrama
  descripcion varchar2(200) not null,
  costo number(7,2) not null,
  constraint paquete_tv_pk primary key(paquete_tv_id),
  constraint paquete_tv_clave_chk check ( clave in('BA', 'IN', 'CO', 'PO'))
);

create table servicio (
  servicio_id number(10,0),
  numero_cuenta number(10,0) not null,
  tarifa_mensual number(6,2) not null, --ToDo: cambiar tipo en diagrama
  forma_pago varchar2(1) default 'E',
  fecha_corte date default sysdate,
  fecha_instalacion date not null,
  es_tv number(1,0) not null,
  es_telefono number(1,0) not null,
  es_internet number(1,0) not null,
  fecha_estatus date default sysdate,
  estatus_servicio_id number(10) not null,
  cliente_id number(10) not null,
  anios_contrato number(2, 0) not null,
  contrato_fin as (fecha_instalacion + anios_contrato*365) virtual,
  constraint servicio_pk primary key(servicio_id),
  constraint servicio_estatus_servicio_id_fk foreign key(estatus_servicio_id)
    references estatus_servicio(estatus_servicio_id) on delete cascade,
  constraint servicio_cliente_id_fk foreign key(cliente_id)
    references cliente(cliente_id) on delete cascade,
  constraint servicio_forma_pago_chk check ( forma_pago in ('T', 'E') )
);

create table pago (
  folio number(10,0),
  servicio_id number(10,0),
  fecha date not null,
  importe number(6,2),
  constraint pago_servicio_id_fk foreign key(servicio_id)
    references servicio(servicio_id) on delete cascade,
  constraint pago_pk primary key(folio, servicio_id)
);

create table estatus_servicio_historial (
  estatus_servicio_historial_id number(10,0),
  fecha_estatus date not null,
  servicio_id number(10,0) not null,
  estatus_servicio_id number(10,0) not null,
  constraint esh_pk primary key(estatus_servicio_historial_id),
  constraint esh_servicio_id_fk foreign key(servicio_id)
    references servicio(servicio_id) on delete cascade,
  constraint esh_estatus_servicio_id_fk foreign key(estatus_servicio_id)
    references estatus_servicio(estatus_servicio_id) on delete cascade
);

create table servicio_tv (
  servicio_id number(10,0),
  numero_decodificadores number(2,0) not null,
  paquete_tv_id number(2,0) not null,
  constraint stv_servicio_id_fk foreign key(servicio_id)
    references servicio(servicio_id) on delete cascade,
  constraint stv_paquete_tv_id_fk foreign key(paquete_tv_id)
    references paquete_tv(paquete_tv_id) on delete cascade,
  constraint stv_pk primary key(servicio_id)
);

create table servicio_telefono (
  servicio_id number(10,0),
  numero_llamadas number(5,0) not null,
  numero_asignado number(10,0) not null,
  constraint stel_servicio_id_fk foreign key(servicio_id)
    references servicio(servicio_id) on delete cascade,
  constraint stel_pk primary key(servicio_id),
  constraint stel_numero_asginado_uk unique (numero_asignado)
);

create table servicio_internet (
  servicio_id number(10,0),
  velocidad_bajada number(4,0) not null,
  velocidad_subida as (velocidad_bajada*0.10) virtual, -- Derivado 10% de bajada
  nombre_correo varchar2(40) not null,
  password_correo varchar2(100) not null,
  constraint sint_servicio_id_fk foreign key(servicio_id)
    references servicio(servicio_id) on delete cascade,
  constraint sint_pk primary key(servicio_id),
  constraint sint_nombre_correo_uk unique (nombre_correo)
);

create table servicio_tv_canal (
  servicio_tv_canal_id number(10,0),
  canal_id number(10,0) not null,
  servicio_id number(10,0) not null,
  constraint sertvcan_pk primary key (servicio_tv_canal_id),
  constraint sertvcan_canal_id_fk foreign key(canal_id)
    references canal(canal_id) on delete cascade,
  constraint sertvcan_servicio_id_fk foreign key(servicio_id)
    references servicio(servicio_id) on delete cascade
);

create table paquete_canal_tv (
  paquete_canal_tv_id number(10,0),
  canal_id number(10,0) not null,
  paquete_tv_id number(2,0) not null,
  constraint pctv_pk primary key(paquete_canal_tv_id),
  constraint pctv_canal_id_fk foreign key(canal_id)
    references canal(canal_id) on delete cascade,
  constraint pctv_paquete_tv_id_fk foreign key(paquete_tv_id)
    references paquete_tv(paquete_tv_id) on delete cascade,
  constraint paquete_canal_tv unique (paquete_tv_id, canal_id)
);

prompt Listo!

disconnect
