--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion: 4/12/2018
--@Descripción: Proyecto Final. Prueba para el procedimiento
-- almacenado del cambio de status

@@s-13-p-adeudo.sql

prompt ============ Prueba procedimiento almacenado  ============

prompt Conectando como administrador
connect o_proy_admin

set serveroutput on

declare
  v_estatusNombre estatus_servicio.nombre%type;
  cursor cur_servicio is
    select * from servicio
    where estatus_servicio_id=(select estatus_servicio_id
                                from estatus_servicio
                                where nombre='SUSPENDIDO') and
          sysdate-fecha_estatus > 365;
begin
  dbms_output.put_line('Registros antes del procedimiento: ');
  for s in cur_servicio loop
    select nombre
      into v_estatusNombre
      from estatus_servicio
      where estatus_servicio_id=s.estatus_servicio_id;
    dbms_output.put_line(s.servicio_id || ' ' || s.fecha_estatus || ' ' || v_estatusNombre);
  end loop;

  concluirConAdeudo();

  dbms_output.put_line('Registros despues del procedimiento: ');
  for s in cur_servicio loop
    select nombre
      into v_estatusNombre
      from estatus_servicio
      where estatus_servicio_id=s.estatus_servicio_id;
    dbms_output.put_line(s.cliente_id || ' ' || s.fecha_estatus || ' ' || v_estatusNombre);
  end loop;

end;
/
show errors
disconnect
