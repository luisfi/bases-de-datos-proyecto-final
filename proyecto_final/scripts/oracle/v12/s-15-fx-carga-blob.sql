--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion: 4/12/2018
--@Descripción: Proyecto Final. Funcion que carga un archivo blob

prompt ============ Funcion que carga objeto blob ============

prompt Conectando como administrador
connect o_proy_admin

set serveroutput on

create or replace function leer_imagen(v_directorio varchar2, v_nombreArchivo in varchar2)
  return blob is

  v_bfile bfile;
  v_src_offset number := 1;
  v_dest_offset number := 1;
  v_dest_blob blob;
  v_src_length number;
  v_dest_length number;
begin
  v_dest_blob := empty_blob();
  dbms_lob.createtemporary(lob_loc => v_dest_blob, cache => true, dur => dbms_lob.call);
  -- dbms_output.put_line('Cargando archivo: ' || v_nombreArchivo);
  v_bfile := bfilename(v_directorio, v_nombreArchivo);
  if dbms_lob.fileexists(v_bfile) = 1 and not dbms_lob.isopen(v_bfile) = 1 then
    dbms_lob.open(v_bfile, dbms_lob.lob_readonly);
  else
    raise_application_error(-200001, 'El archivo ' || v_nombreArchivo ||
    ' no existe en el directorio '||v_directorio);
  end if;
  dbms_lob.loadblobfromfile(
    dest_lob => v_dest_blob,
    src_bfile => v_bfile,
    amount => dbms_lob.getlength(v_bfile),
    dest_offset => v_dest_offset,
    src_offset => v_src_offset);
  dbms_lob.close(v_bfile);

  v_src_length := dbms_lob.getlength(v_bfile);
  v_dest_length := dbms_lob.getlength(v_dest_blob);
  if v_src_length != v_dest_length then
    raise_application_error(-20002,'Error al escribir datos.\n'
      ||' Se esperaba escribir '||v_src_length
      ||' Pero solo se escribio '||v_dest_length);
--   else
--     dbms_output.put_line('Escritura correcta, bytes escritos: '
--     || v_src_length);
  end if;
  return v_dest_blob;
end;
/
show errors

disconnect
