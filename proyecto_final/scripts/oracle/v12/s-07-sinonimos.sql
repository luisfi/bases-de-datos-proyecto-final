--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion:  29/11/2018
--@Descripcion: Proyecto Final. Creacion de sinonimos para que otros
-- usuarios puedan visualizar datos


prompt ============ Creando sinonimos ============

prompt Conectando como administrador
connect o_proy_admin

whenever sqlerror exit rollback

create or replace public synonym estado for o_proy_admin.estado;
create or replace public synonym municipio for o_proy_admin.municipio;
create or replace public synonym paquete_tv for o_proy_admin.paquete_tv;

prompt Otorgando permisos de lectura a usuario invitado
grant select on canal to o_proy_invitado;
grant select on cliente to o_proy_invitado;
grant select on direccion to o_proy_invitado;

prompt Dando permisos de creacion de sinonimos privados a usuario invitado

prompt Conectando como SYS
connect sys as sysdba
grant create synonym to o_proy_invitado;

prompt Conectando como invitado
connect o_proy_invitado

create or replace synonym canal for o_proy_admin.canal;
create or replace synonym cliente for o_proy_admin.cliente;
create or replace synonym direccion for o_proy_admin.direccion;

prompt Creando sinonimos para el software
prompt Conectando como administrador
connect o_proy_admin

set serveroutput on

declare
  cursor nombre_tabla is
  select table_name from user_tables;

begin
  for n in nombre_tabla loop
    dbms_output.put_line('Creando sinonimo ' || 'ov_' || n.table_name);
    execute immediate 'create or replace synonym ov_' || lower(n.table_name) || ' for ' || lower(n.table_name);
  end loop;
end;
/
show errors
prompt Listo!

disconnect
