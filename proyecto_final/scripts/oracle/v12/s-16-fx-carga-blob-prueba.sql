--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion: 4/12/2018
--@Descripción: Proyecto Final. Prueba a la funcion que carga
-- archivos blob modificando la tabla direccion agregando columna blob

@@s-15-fx-carga-blob.sql

prompt ============ Prueba Funcion  ============


prompt Conectando como SYS
connect sys as sysdba


  prompt Creando directorio para imagenes
create or replace directory img_dir as '/proyecto_final/img';

  prompt Dando permisos de lectura al directorio al usuario administrador
grant read on directory img_dir to o_proy_admin;

prompt Conectando como administrador
connect o_proy_admin

whenever sqlerror exit rollback
set serveroutput on

prompt Agregando columna blob
declare
  v_numColumnas number := 0;
begin
  select count(*)
    into v_numColumnas
    from user_tab_cols
    where column_name = 'CREDENCIAL_IMG' and
          table_name = 'DIRECCION';

  if (v_numColumnas = 0) then
    execute immediate 'alter table direccion add (credencial_img blob);';
  else
    dbms_output.put_line('La columna ya existe.');
  end if;
end;
/

prompt Almacenando imagenes de credenciales
declare
  v_dirNnombre cliente.nombre%type := 'IMG_DIR';
  v_imgNombre cliente.nombre%type;
  cursor cur_cliente is
    select cliente_id,
           nombre
      from cliente;
  cursor cur_direccion is
    select cliente_id,
           numero
      from direccion;
begin
  for c in cur_cliente loop
    v_imgNombre := c.NOMBRE || '.jpg';
    for d in cur_direccion loop
      if d.cliente_id=c.cliente_id then
        dbms_output.put_line('Cliente: '|| c.cliente_id ||  ' direccion:  '||d.numero);
        update direccion set credencial_img=leer_imagen(v_dirNnombre, v_imgNombre)
        where cliente_id=c.cliente_id;
      end if;
    end loop;
  end loop;
end;
/
show errors

disconnect
