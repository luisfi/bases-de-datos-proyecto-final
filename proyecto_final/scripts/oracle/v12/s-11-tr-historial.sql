--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion: 4/12/2018
--@Descripción: Proyecto Final. Trigger que almacena el estatus del servicio en el historial

prompt ============ Trigger cambios en historial ============


prompt Conectando como administrado
connect o_proy_admin

create or replace trigger trg_estatus_servicio_historial
  after insert or update of estatus_servicio_id on servicio
  for each row
  declare
    v_estatusID number;
    v_fechaEstatus date;
    v_historialID number;
    v_servicioID  number;
  begin
    v_historialID := estatus_servicio_historial_seq.nextval;
    v_estatusID := :new.estatus_servicio_id;
    v_fechaEstatus := :new.fecha_estatus;
    v_servicioID := :new.servicio_id;
    dbms_output.put_line('Estatus anterior: ' || :old.estatus_servicio_id);
    dbms_output.put_line('Estatus nuevo: ' || :new.estatus_servicio_id);

    insert into estatus_servicio_historial (estatus_servicio_historial_id, fecha_estatus,
                                            servicio_id, estatus_servicio_id)
    values(v_historialID, v_fechaEstatus, v_servicioID, v_estatusID);
  end;
/
show errors

disconnect
