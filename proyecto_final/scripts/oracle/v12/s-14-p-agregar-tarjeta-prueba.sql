--@Autor: Luis Alberto Oropeza Vilchis
--@Fecha creacion: 4/12/2018
--@Descripción: Proyecto Final. Prueba procedimiento que agrega
-- pago con tarjeta de credito

@s-13-p-agregar-tarjeta.sql

prompt ============ Prueba procedimiento almacenado  ============

prompt Conectando como administrador
connect o_proy_admin

set serveroutput on

declare
  v_clienteID number;
  v_numTarjetas number;
  cursor cur_tarjeta is
    select * from tarjeta;
begin
  select min(c.cliente_id)
    into v_clienteID
    from cliente c
    join servicio s
    on c.cliente_id=s.cliente_id
    where s.forma_pago='E';
  select count(*) into v_numTarjetas from tarjeta where cliente_id=v_clienteID;
  dbms_output.put_line('Cliente ' || v_clienteID || ' --> ' || v_numTarjetas || ' tarjetas');
  agregarTarjetaCliente(v_clienteID, '111122223333'||random_integer(1000,9999), 'VISA', '000');
  select count(*) into v_numTarjetas from tarjeta where cliente_id=v_clienteID;
  for t in cur_tarjeta loop
    if t.cliente_id = v_clienteID then
      dbms_output.put_line('Tarjeta ' || t.tarjeta_id || ' ' || t.numero || ' ' || t.tipo);
    end if;
  end loop;
  dbms_output.put_line('Cliente ' || v_clienteID || ' --> ' || v_numTarjetas || ' tarjetas');
end;
/
disconnect
