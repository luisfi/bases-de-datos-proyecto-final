Proyecto final Bases de Datos
=============================

Bases de Datos. Plan 2010 Facultad de Ingeniería UNAM.

* [Especificaciones](doc/especificaciones.pdf)
* [Caso de estudio](doc/caso-estudio.md)
* [Oracle](scripts/oracle/v12)
* [Aplicación](web)

### Todo

* Migrar código a Postgres
* Migrar código a MariaDB
* Mejorar aplicación web para interactuar con la base de datos
