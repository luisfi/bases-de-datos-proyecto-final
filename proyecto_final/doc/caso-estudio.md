iNet
====
[Documento original](caso-estudio.pdf)

iNet es una empresa de telecomunicaciones que ofrece servicios de televisión,
telefonía e internet. La empresa está próxima a iniciar operaciones y
requiere construir una base de datos para soportar sus operaciones.

La empresa ofrece a sus clientes:

* Servicio de Televisión
* Servicio de Telefonía
* Servicio de Internet

El cliente tiene la libertad de elegir uno o hasta los tres
servicios. Para cada servicio sin importar las modalidades seleccionadas
se requiere registrar la siguiente información:

* Número de cuenta de 13 caracteres
* Fecha de instalación.
* Tarifa mensual a cobrar por los servicios seleccionados
* Fecha de corte.
* Forma de pago (T=tarjeta de crédito o E=efectivo). Es suficiente
con almacenar el caracter E o T.

### Servicio de Televisión
Se requiere registrar el paquete de televisión seleccionado por el cliente:

* BA - Básico
* IN - Intermedio
* CO - Completo
* PO - Personalizado

Cada paquete tiene un costo mensual pre-establecido.
Cada paquete tiene su lista de canales. iNet cuenta con un catálogo de
canales y se almacena:

* Número de canal
* Nombre
* Descripción del canal
* Tipo de canal:
  * A - Para todo público
  * B - Para adolecentes y adultos
  * C - Para adultos.

Para el caso del paquete personalizado, el cliente
selecciona la lista de canales que desea adquirir (hasta 150 canales).
Para este servicio también se requiere registrar el número de
decodificadores de televisión que se instalarán en el domicilio.

### Servicio de Telefonía
Para el servicio de telefonía, se registra:

* Número asignado por la empresa (en caso de haber solicitado portabilidad,
  se registra  el número de teléfono proporcionado por el cliente).
* Número máximo de llamadas por mes tanto nacionales como al extranjero.

### Servicio de Internet
Para el caso del servicio de internet, se requiere registrar:

* Velocidad de bajada en Mb/s
* Velocidad de subida en Mb/s.

Al contratar este servicio, iNet le proporciona al cliente un correo electrónico
con el dominio @inet.com. Se registra este correo electrónico, asi como la
contraseña correspondiente.

### Consideraciones

Para realizar el pago de los servicios, el cliente puede proporcionar los datos
de una tarjeta de crédito. Se le solicita:

* Número de tarjeta
* Tipo (no requiere catálogo)
* Número de seguridad.

Los datos generales a registrar del cliente son:

* Nombre
* Apellido paterno
* Apellido materno (opcional)
* RFC
* CURP (opcional)

El cliente puede registrar hasta 2 direcciones. La dirección 1 corresponde con
la dirección del domicilio donde se realizará la instalación, y la
dirección 2 corresponde con su dirección fiscal. En algunos casos, la dirección
fiscal es la misma que la de su domicilio, por lo que solo se registra una.
Para ambos tipos de registra:
* Calle
* Número exterior
* Número interior
* Colonia
* Delegación o Municipio
* C.P.
* Estado o Entidad.

La empresa solo cuenta con catálogos de Estados y Municipios/Alcaldías.

Para llevar el control de los pagos del cliente, cada mes se debe registrar
el pago realizado por el cliente. Se genera un folio de 18 digitos (consecutivos
iniciando en 1 por cada cliente), la fecha de pago y el importe.

Finalmente, iNet desea registrar el estado actual y los cambios de estado que
sufren los servicios de sus clientes a través del tiempo: cuando el servicio se
contrata por primera vez, se le considera como CREADO. A partir del primer pago
del cliente, el servicio se considera como VIGENTE. Si el cliente se atrasa en
sus pagos, el servicio de actualiza a CON ADEUDO. Si después de 15 días de atraso,
el cliente no cubre sus adeudos, el servicio se marca como SUSPENDIDO. Cuando
el cliente paga su adeudo, el servicio se reanuda y se considera como VIGENTE.
Si pasa más de un año y el servicio sige marcado como SUSPENDIDO, el sistema
actualiza su valor a CONCLUIDO CON ADEUDO, es decir el cliente nunca cubre su
adeudo y la empresa inicia acciones legales en contra del excliente. Finalmente,
el servicio se puede dar por CONCLUIDO a petición del cliente siempre y cuando
el servicio no tenga adeudos.

### Diagrama de estados del historial

![Diagrama de estados](doc/img/estados.png)

### Diagrama conceptual

![Diagrama conceptual](doc/img/conceptual.png)

### Diagrama relacional

![Diagrama relacional](doc/img/logico.jpg)
