from django.shortcuts import render
from django.http import HttpResponse
from .models import Cliente, Servicio
import logging
import random

logger = logging.getLogger(__name__)

def index(request):
    return render(request, 'administracion/index.html')

def contratar(request):
    logger.debug('Regresando contratar')
    return render(request, 'administracion/contratar.html')

def clientes(request):
    c = Cliente.objects.order_by('nombre')
    context = {'clientes':c,}
    return render(request, 'administracion/clientes.html', context)

def servicios(request):
    s = Servicio.objects.order_by('servicio_id')
    context = {'servicios':s,}
    return render(request, 'administracion/servicios.html', context)

def registrar(request):
    cliente = Cliente.objects.create(cliente_id=random.randint(1000, 20000), nombre=request.POST['nombre'],
    apellido_paterno=request.POST['ap_paterno'], apellido_materno=request.POST['ap_materno'],
    rfc=request.POST['rfc'], curp=request.POST['curp'])
    return render(request, 'administracion/registro_correcto.html')
