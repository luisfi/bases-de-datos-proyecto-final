from django.contrib import admin
from .models import Cliente,Estado,Municipio,Canal
# Register your models here.

admin.site.register(Cliente)
admin.site.register(Estado)
admin.site.register(Municipio)
admin.site.register(Canal)

