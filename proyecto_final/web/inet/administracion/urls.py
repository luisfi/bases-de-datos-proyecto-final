
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
	path('contratar/',views.contratar, name='contratar'),
    path('clientes/', views.clientes, name='clientes'),
    path('servicios/', views.servicios, name='servicios'),
    path('registrar/', views.registrar, name='registrar'),
    ]
