# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128, blank=True, null=True)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150, blank=True, null=True)
    first_name = models.CharField(max_length=30, blank=True, null=True)
    last_name = models.CharField(max_length=150, blank=True, null=True)
    email = models.CharField(max_length=254, blank=True, null=True)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Canal(models.Model):
    canal_id = models.IntegerField(primary_key=True)
    numero = models.IntegerField()
    nombre = models.CharField(max_length=40)
    descripcion = models.CharField(max_length=400)
    tipo_canal = models.ForeignKey('TipoCanal', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'canal'


class Cliente(models.Model):
    cliente_id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=40)
    apellido_paterno = models.CharField(max_length=40)
    apellido_materno = models.CharField(max_length=40, blank=True, null=True)
    rfc = models.CharField(max_length=13)
    curp = models.CharField(max_length=18, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cliente'


class ClientePreferente(models.Model):
    cliente_id = models.IntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=40, blank=True, null=True)
    apellido_paterno = models.CharField(max_length=40, blank=True, null=True)
    apellido_materno = models.CharField(max_length=40, blank=True, null=True)
    numero_tarjeta = models.CharField(max_length=16, blank=True, null=True)
    tipo_tarjeta = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cliente_preferente'


class Direccion(models.Model):
    numero = models.BooleanField()
    cliente = models.ForeignKey(Cliente, models.DO_NOTHING, primary_key=True)
    calle = models.CharField(max_length=40)
    colonia = models.CharField(max_length=40)
    numero_interior = models.IntegerField(blank=True, null=True)
    numero_exterior = models.IntegerField()
    codigo_postal = models.IntegerField()
    municipio = models.ForeignKey('Municipio', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'direccion'
        unique_together = (('cliente', 'numero'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200, blank=True, null=True)
    action_flag = models.IntegerField()
    change_message = models.TextField(blank=True, null=True)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100, blank=True, null=True)
    model = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField(blank=True, null=True)
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Estado(models.Model):
    estado_id = models.IntegerField(primary_key=True)
    clave = models.CharField(max_length=3)
    nombre = models.CharField(max_length=40)

    class Meta:
        managed = False
        db_table = 'estado'


class EstatusServicio(models.Model):
    estatus_servicio_id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=40)
    descripcion = models.CharField(max_length=400)

    class Meta:
        managed = False
        db_table = 'estatus_servicio'


class EstatusServicioHistorial(models.Model):
    estatus_servicio_historial_id = models.IntegerField(primary_key=True)
    fecha_estatus = models.DateField()
    servicio = models.ForeignKey('Servicio', models.DO_NOTHING)
    estatus_servicio = models.ForeignKey(EstatusServicio, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'estatus_servicio_historial'


class Municipio(models.Model):
    municipio_id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=40, blank=True, null=True)
    clave = models.CharField(max_length=40)
    estado = models.ForeignKey(Estado, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'municipio'


class Pago(models.Model):
    folio = models.IntegerField(primary_key=True)
    servicio = models.ForeignKey('Servicio', models.DO_NOTHING)
    fecha = models.DateField()
    importe = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pago'
        unique_together = (('folio', 'servicio'),)


class PaqueteCanalTv(models.Model):
    paquete_canal_tv_id = models.IntegerField(primary_key=True)
    canal = models.ForeignKey(Canal, models.DO_NOTHING)
    paquete_tv = models.ForeignKey('PaqueteTv', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'paquete_canal_tv'
        unique_together = (('paquete_tv', 'canal'),)


class PaqueteTv(models.Model):
    paquete_tv_id = models.IntegerField(primary_key=True)
    clave = models.CharField(max_length=2)
    descripcion = models.CharField(max_length=200)
    costo = models.DecimalField(max_digits=7, decimal_places=2)

    class Meta:
        managed = False
        db_table = 'paquete_tv'


class ReporteServicio(models.Model):
    servicio_id = models.IntegerField(blank=True, null=True)
    tarifa_mensual = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    forma_pago = models.CharField(max_length=1, blank=True, null=True)
    numero_decodificadores = models.IntegerField(blank=True, null=True)
    numero_llamadas = models.IntegerField(blank=True, null=True)
    velocidad_bajada = models.IntegerField(blank=True, null=True)
    velocidad_subida = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'reporte_servicio'


class Servicio(models.Model):
    servicio_id = models.IntegerField(primary_key=True)
    numero_cuenta = models.IntegerField()
    tarifa_mensual = models.DecimalField(max_digits=6, decimal_places=2)
    forma_pago = models.CharField(max_length=1, blank=True, null=True)
    fecha_corte = models.DateField(blank=True, null=True)
    fecha_instalacion = models.DateField()
    es_tv = models.BooleanField()
    es_telefono = models.BooleanField()
    es_internet = models.BooleanField()
    fecha_estatus = models.DateField(blank=True, null=True)
    estatus_servicio = models.ForeignKey(EstatusServicio, models.DO_NOTHING)
    cliente = models.ForeignKey(Cliente, models.DO_NOTHING)
    anios_contrato = models.IntegerField()
    contrato_fin = models.DateField()

    class Meta:
        managed = False
        db_table = 'servicio'


class ServicioInternet(models.Model):
    servicio = models.ForeignKey(Servicio, models.DO_NOTHING, primary_key=True)
    velocidad_bajada = models.IntegerField()
    velocidad_subida = models.IntegerField()
    nombre_correo = models.CharField(unique=True, max_length=40)
    password_correo = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'servicio_internet'


class ServicioTelefono(models.Model):
    servicio = models.ForeignKey(Servicio, models.DO_NOTHING, primary_key=True)
    numero_llamadas = models.IntegerField()
    numero_asignado = models.IntegerField(unique=True)

    class Meta:
        managed = False
        db_table = 'servicio_telefono'


class ServicioTv(models.Model):
    servicio = models.ForeignKey(Servicio, models.DO_NOTHING, primary_key=True)
    numero_decodificadores = models.IntegerField()
    paquete_tv = models.ForeignKey(PaqueteTv, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'servicio_tv'


class ServicioTvCanal(models.Model):
    servicio_tv_canal_id = models.IntegerField(primary_key=True)
    canal = models.ForeignKey(Canal, models.DO_NOTHING)
    servicio = models.ForeignKey(Servicio, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'servicio_tv_canal'


class SucursalesExt(models.Model):
    sucursal_id = models.IntegerField(blank=True, null=True)
    estado = models.CharField(max_length=40, blank=True, null=True)
    municipio = models.CharField(max_length=40, blank=True, null=True)
    fecha_apertura = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sucursales_ext'


class Tarjeta(models.Model):
    tarjeta_id = models.IntegerField(primary_key=True)
    numero = models.CharField(max_length=16)
    tipo = models.CharField(max_length=25)
    numero_seguridad = models.IntegerField()
    cliente = models.ForeignKey(Cliente, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'tarjeta'


class TipoCanal(models.Model):
    tipo_canal_id = models.IntegerField(primary_key=True)
    clave = models.CharField(max_length=5)
    descripcion = models.CharField(max_length=400)

    class Meta:
        managed = False
        db_table = 'tipo_canal'
