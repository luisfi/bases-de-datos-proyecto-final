Prácticas
=========

Se encuentran las prácticas realizadas en la asignatura de Bases de Datos,
cada una en su folder correspodiente (P*X*, donde X se reemplaza por el número
de práctica). Para cada práćtica se encuentra el archivo de las
especificaciones (*pX_req.pdf*), el reporte realizado (*pX_reporte.pdf*) y en
algunos casos un archivo que especifica una investigación previa
(*pX_previo.pdf*)
así como algunos archivos (*.sql*, *.plb*) necesarios para poder realizarla.

* [Práctica 0: Instalación del Sistema Operativo](P0/)
* [Práctica 1: Instalación del software de Oracle](P1/)
* [Práctica 2: Creación de una Base de Datos con Oracle 12](P2/)
* [Práctica 3: Diseño de modelos básicos E/R con notación Chen](P3/)
* [Práctica 4: Diseño de modelos avanzados E/R con notación Chen](P4/)
* [Práctica 5: Diseño básico de modelos relacionales](P5/)
* [Práctica 6: Diseño avanzado de modelos relacionales](P6/)
* [Práctica 7: SQL Plus y lenguaje de definición de datos (DDL) ](P7/)
* [Práctica 8: Instalación de IDE's y lenguaje de manipulación de datos (DML) ](P8/)
* [Práctica 9: Sentencia SELECT](P9/)
* [Práctica 10: Operadores SQL](P10/)
* [Práctica 11: Funciones de agregación y subconsultas](P11/)
