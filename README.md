Bases de Datos
==============

Recursos generados para la asignatura bases de datos de la Faculta de Ingeniería
de la UNAM, plan 2010.

En el repositorio se encuentran:

1. [Prácticas](practicas/)

2. [Proyecto Final](proyecto_final/)

Que sirvan de apoyo para la comunidad.

### Licencia
Todo el código generado en este repositorio está bajo la licencia [GPLv3](LICENCE.md)
